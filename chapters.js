const file = 'yeatmix.mp3'
const chapterFile = 'chatpers.txt'

const fs = require('node:fs')
const ffmpeg = require('fluent-ffmpeg')


// return length of file (for final chapter)
function getLength () {
  return new Promise((resolve, reject) => {
    ffmpeg.ffprobe('./yeatmix.mp3', function(err, metadata) {
      if (err) reject(err)
      resolve(new Date(metadata.format.duration* 1000).toISOString().slice(11, 19))
    })
  })
}


const getTracks = async () => {
  // get total length of file, for final length
  const finalLen = await getLength()

  // set of tracks with start and end
  const tracks = []

  const fsPromises = require('fs').promises
  const chapters = await fsPromises.readFile('chapters.txt')

  const array = chapters.toString().split("\n").filter((e) => e != '')

  let i = 0
  while (i < array.length) {
    // split ts/title out
    const match = array[i].match(/^(.*?) (.*)/)
    if (i+1 != array.length) {
      // get start time of next track
      const matchNxt = array[i+1].match(/^(.*?) (.*)/)
      tracks.push([match[1], matchNxt[1], match[2].trim()])
      i++
    } else { // final track, use total length
      tracks.push([match[1], finalLen, match[2].trim()])
      // console.log(tracks)
      return tracks
    }
  }
}


(async () => {
  let tracks = await getTracks()
  console.log(tracks)
})();
